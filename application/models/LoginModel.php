<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LoginModel extends CI_Model
{

    private $table_name = "user";
    private $table_role = "user_role";

    public $id;
    public $name;
    public $email;
    public $image = "default.jpg";
    public $role_id = 2;
    public $is_active = 1;
    public $date_created;

    public function rules()
    {
        return [
            // [
            //     'field' => 'email',
            //     'label' => 'Email',
            //     'rules' => 'required|trim|valid_email'
            // ], // rule untuk field email
            [
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|trim'
            ], // rule untuk field name
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|trim'
            ]
        ];
    }
    public function getById($id)
    {
        return $this->db->get_where(
            $this->table_role,
            ["id" => $id]
        )->result_array();
    }

    public function getByEmail($email)
    {
        return $this->db->get_where(
            $this->table_name,
            ["email" => $email]
        )->result_array();
    }

    public function prosesLogin()
    {
        $this->_login();
    }
    private function _login()
    {
        $name = $this->input->post('name');
        $password = $this->input->post('password');
        // ngambil data user dengan where
        $user = $this->db->get_where($this->table_name, ['name' => $name])->row_array();

        // jika user ada
        if ($user) {
            // jika user aktif
            if ($user['is_active'] == 1) {
                //    cek password
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'email' => $user['email'],
                        'role_id' => $user['role_id']
                    ];
                    // membuat session dari login user
                    $this->session->set_userdata($data);
                    // pengecekan role user 
                    if ($user['role_id'] == 1) {
                        redirect('home');
                    } else {
                        redirect('user');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Password Anda Salah!</div>');
                    redirect('Auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Email belum di Aktifkan!</div>');
                redirect('Auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Kamu Gagal login/Email belum terdaftar!</div>');
            redirect('Auth');
        }
    }
}



/* End of file LoginModel.php */
