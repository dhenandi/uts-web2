<div class="container">
    <form class="form-login" action="<?= base_url('auth') ?>" method="POST">
        <h2 class="form-login-heading">sign in now</h2>
        <?= $this->session->flashdata('message'); ?>
        <div class="login-wrap">
            <input type="text" class="form-control" name="name" id="name" placeholder="User ID" autofocus value="<?= set_value('name'); ?>">
            <?= form_error('name', ' <small class="text-danger pl-3">', '</small>'); ?>

            <br>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
            <?= form_error('password', ' <small class="text-danger pl-3">', '</small>'); ?>
            <br>

            <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button><br>
            <p class="text-center">username : admin - password : admin</p>
            <p class="text-center">username : user - password : user</p>
            <br>
            <div class="registration">
                Don't have an account yet?<br />
                <a data-toggle="modal" href="#myModal">
                    Create an account
                </a>
            </div>
        </div>
    </form>
    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
            <form action="<?= base_url('auth/registrasi'); ?>" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br>
                        <h4 class="modal-title">create account?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your new account</p>
                        <input type="text" name="name" id="name" placeholder="Full Name" autocomplete="off" class="form-control placeholder-no-fix" value="<?= set_value('name'); ?>">
                        <?= form_error('name', ' <small class="text-danger pl-3">', '</small>'); ?>
                        <br>
                        <input type="text" name="email" id="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix" value="<?= set_value('email'); ?>">
                        <?= form_error('email', ' <small class="text-danger pl-3">', '</small>'); ?>
                        <br>
                        <input type="password" name="password1" id="password1" placeholder="Password" autocomplete="off" class="form-control placeholder-no-fix">
                        <?= form_error('password1', ' <small class="text-danger pl-3">', '</small>'); ?>
                        <br>
                        <input type="password" name="password2" id="password2" placeholder="Repeat Password" autocomplete="off" class="form-control placeholder-no-fix"><br>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-theme" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- modal -->
</div>