 <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
 <!--sidebar start-->
 <aside>
     <div id="sidebar" class="nav-collapse ">
         <!-- sidebar menu start-->
         <ul class="sidebar-menu" id="nav-accordion">
             <?php
                if ($this->session->userdata('role_id') == 1) :
                ?>
                 <?php foreach ($user as $u); ?>
                 <p class="centered"><a href="<?= base_url('user') ?>"><img src="<?= base_url('assets/img/') . $u['image'] ?>" class="img-circle" width="80"></a></p>
                 <h5 class="centered"><?= $u['name']; ?></h5>
                 <?php foreach ($role as $r); ?>
                 <li class="sitebar-heading mb-1 mt-4" style="color: blue"><?= $r['role']; ?></li>
                 <li class="mt">
                     <a class="" href="<?= base_url('home') ?>">
                         <i class="fa fa-dashboard"></i>
                         <span>Dashboard</span>
                     </a>
                 </li>
                 <li class="sub-menu">
                     <a href="javascript:;">
                         <i class="fa fa-desktop"></i>
                         <span>Profile</span>
                     </a>
                     <ul class="sub">
                         <li><a href="<?= base_url('user') ?>">Profile</a></li>
                         <li><a href="<?= base_url('user') ?>">Edit Profile</a></li>
                     </ul>
                 </li>
             <?php else : ?>
                 <?php foreach ($role as $r); ?>
                 <li class="sitebar-heading mb-1 mt-4" style="color: blue"><?= $r['role']; ?></li>
                 <li class="sub-menu">
                     <a href="javascript:;">
                         <i class="fa fa-desktop"></i>
                         <span>Profile</span>
                     </a>
                     <ul class="sub">
                         <li><a href="<?= base_url('user') ?>">Profile</a></li>
                         <li><a href="<?= base_url('user') ?>">Edit Profile</a></li>
                     </ul>
                 </li>
             <?php endif ?>
         </ul>
         <!-- sidebar menu end-->
     </div>
 </aside>
 <!--sidebar end-->