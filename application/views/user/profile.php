<!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12 main-chart">
                <!--CUSTOM CHART START -->
                <div class="border-head">
                    <h3>USER VISITS</h3>
                </div>
                <div class="card mb-3" style="max-width: 640px;">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <?php foreach ($user as $u); ?>
                            <img src="<?= base_url('assets/img/') . $u['image'] ?>" class="card-img" alt="...">
                        </div>
                        <div class="col-md-6 mt-5">
                            <div class="card-body">
                                <h5 class="card-title"><?= $u['name']; ?></h5>
                                <p class="card-text"><?= $u['email']; ?></p>
                                <p class="card-text"><small class="text-muted">Member since <?= date('d F Y', $u['date_created']); ?></small></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
        <!-- /row -->
    </section>
</section>
<!--main content end-->