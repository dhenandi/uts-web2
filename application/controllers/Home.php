<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('LoginModel', 'user');
        is_logged_in(); 
    }

    public function index()
    {
        $email = $this->session->userdata('email');
        $id = $this->session->userdata('role_id');
        $data = array(
            "title" => "Dashboard",
            "content" => "default",
            "user" => $this->user->getByEmail($email),
            "role" => $this->user->getById($id)
        );
        $this->load->view('wrapper', $data);
    }
}

/* End of file Home.php */
