<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    // Constructor default
    public function __construct()
    {
        parent::__construct();
        $this->load->model('LoginModel', 'auth');
        $this->load->library('form_validation');
    }


    public function index()
    {
        // Pengecekan terkait autentikasi, apabila sudah login, maka akan di alihkan ke dashboard
        if ($this->session->userdata('email')) {
            redirect('dashboard');
        }

        // Pembuatan validasi login
        // Ada if else dan form validation
        $auth = $this->auth; 
        $validation = $this->form_validation; 
        $validation->set_rules($auth->rules());
        if ($validation->run() == false) {
            $data = array(
                "title" => "Halaman Login",
                "content" => "auth"
            );
            //  Jika login salah akan dimasukkan ke view dari auth_wrapper (isinya halaman auth)
            $this->load->view('auth_wrapper', $data);
        } else {
            // jika proses login benar, maka controller akan mengalihkan ke dashboard dengan mengecek model auth
            $this->auth->prosesLogin();
        }
    }


    public function registrasi()
    {
        if ($this->session->userdata('email')) {
            redirect('home');
        }


        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'Email sudah ada/sudah terdaftar!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
            'matches' => 'Password tidak sama!',
            'min_length' => 'Password terlalu pendek'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        
        if ($this->form_validation->run() == FALSE) {
            $data = array(
                "title" => "Halaman Registrasi",
                "content" => "registrasi"
            );
            $this->load->view('auth_wrapper', $data);
        } else {
            $email = $this->input->post('email', true);
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($email),
                'image' => 'default.jpg',
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'is_active' => 1,
                'date_created' => time()

            ];

            $this->db->insert('user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success"> Akun kamu berhasil terdaftar. silahkan login!</div>');
            redirect('Auth');
        }
    }
    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->set_flashdata('message', '<div class="alert alert-success"> Kamu berhasil logout!</div>');
        redirect('auth');
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }
}
