<?php
// membuat helper sendiri jika user belum login supaya tidak bisa lansung masuk menu menu
function is_logged_in()
{
    $ci = get_instance();
    if (!$ci->session->userdata('email')) {
        redirect('auth');
    }
}
